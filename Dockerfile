FROM ubuntu:18.04
RUN apt-get update && apt-get install nginx -y
RUN echo "daemon off;" >> /etc/nginx/nginx.conf
EXPOSE 80
ENTRYPOINT ["/etc/init.d/nginx", "start"]